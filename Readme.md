#CollapsibleD3Tree#

CollapsibleD3Tree is a simple application that illustrates a way to make AngularJS components with D3.js modules. 

### How do I get set up? ###

* Clone the repository.
* Build the project.
* Deploy the artifact.

### Built with ###
* Maven - Dependency Management